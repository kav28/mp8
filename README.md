# Mini-project 8

## Strava CLI

In this project, I created a command-line interface (CLI) in Rust, called my_strava_cli, to analyze Strava running data stored in a CSV file. The CLI is designed to extract and display the first five (most recent) distances recorded in the CSV file.

### Features

CSV Data Analysis: The CLI parses a CSV file containing Strava running data.
Extract First Five Distances: It extracts the first five distances from the CSV file's 'distance' column.
Testing: Included unit tests to ensure the functionality of the CSV processing function.
Usage

To use this CLI tool, follow these steps:

**Install Rust**: If you haven't already, install Rust from the official website.
Clone the Repository: Clone this repository to your local machine.

```git clone <repository_url>```

**Build the Project**: Navigate to the project directory and build the Rust project using Cargo.

```
cd <project_directory>
cargo build --release
```

**Run the CLI**: Run the compiled executable with the path to your Strava data CSV file as an argument.

```
./target/release/my_strava_cli <path_to_csv_file>
Example
```


Suppose you have a CSV file named strava_data.csv containing Strava running data. To extract the first five distances, you would run:


```
./target/release/my_strava_cli strava_data.csv
Future Enhancements
```


To expand this project, I am considering the following enhancements:

Integration with Strava API: Connect the CLI with the Strava API to fetch real-time data or sync data directly from your Strava account.

Interactive Mode: Implement an interactive mode allowing users to explore various data analysis options and visualize their Strava data.

Extended Data Analysis: Enhance data analysis capabilities to include more statistics and insights beyond just the distance, such as pace, elevation, and heart rate.

Error Handling: Implement more robust error handling and informative error messages to guide users in case of incorrect inputs or file formats.

Improved Testing: Expand unit tests to cover edge cases and ensure comprehensive test coverage for all functions.

Data used in this project sourced from https://www.kaggle.com/datasets/ajitjadhav1/strava-running-activity-data