use csv::ReaderBuilder;
use std::error::Error;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "my_strava_cli", about = "A CLI for analyzing Strava data")]
struct Cli {
    /// Path to the CSV file
    #[structopt(parse(from_os_str))]
    input: PathBuf,
}

// Function to read CSV file and extract the first 5 values in the 'distance' column
fn process_csv_file(file_path: &PathBuf) -> Result<Vec<f64>, Box<dyn Error>> {
    let file = std::fs::File::open(file_path)?;
    let mut rdr = ReaderBuilder::new().from_reader(file);

    let headers = rdr.headers()?;
    let distance_index = headers.iter().position(|h| h == "distance").unwrap();

    let mut distances = Vec::new();
    for result in rdr.records() {
        let record = result?;
        if let Some(distance_str) = record.get(distance_index) {
            if let Ok(distance) = distance_str.parse::<f64>() {
                distances.push(distance);
                if distances.len() == 5 {
                    break;
                }
            }
        }
    }

    Ok(distances)
}

fn main() -> Result<(), Box<dyn Error>> {
    let args = Cli::from_args();
    let distances = process_csv_file(&args.input)?;
    for distance in distances {
        println!("Distance: {}", distance);
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_process_csv_file() {
        let file_path = PathBuf::from("test_data.csv");
        let result = process_csv_file(&file_path);
        assert!(result.is_ok());
        let distances = result.unwrap();
        assert_eq!(distances.len(), 5);
    }
}


